## Stadia | Force 4K

There are two requirements for enjoying a 4K stream on Stadia:

* VP9 decoding support (hardware or software).
* A physical 4K monitor.

This is where this script comes into play. By setting a JavaScript `localStorage`  property and  using `xrandr`'s panning option, we can make it appear to Stadia as if our monitor's physical resolution is 4K (i.e., virtual resolution), and that we have VP9 decoding support.

**Note:** Enforcing VP9 is experimental. Sometimes it requires multiple attempts to work, and sometimes may not work at all. 

## Installation

You'll need the following dependencies:

```bash
sudo apt install -y autorandr xdotool
```

I suggest you install the script somewhere in your `$PATH` directories.

```bash
git clone https://gitlab.com/montazar/stadia
cd stadia
sudo cp stadia.sh /usr/local/bin/stadia 
```



## Use case

1. Open Stadia on your browser:

```bash
xdg-open https://www.stadia.com
```

2. Enforce VP9 encoding (or use the Stadia+ extension).

```bash
stadia vp9
```

3. Open your *Stadia settings* and go to the *Performance* tab. Enter in your terminal:

```bash
stadia performance
```

This should enable the 4K option in the drop-down list. Select it.

4. Now you're ready to start a game. Run the following command: (this will disable all the non-primary displays, `stadia off` will enable them again).

```bash
stadia on
```

5. Start your game and wait until the stream resolution is 4K. Once the stream resolution has been promoted to 4K, you can disable the script.

```bash
stadia off
```


*Note: Due to the way Stadia operates, you may have to repeat this process each time you begin a new game. However, you should not have to enforce VP9 encoding (but it can be required anyway if your connection drops frequently.)*

### Status

You can at anytime check if the script is on:

```bash
stadia status
```

Notice: This assumes your monitor's physical resolution is not actually 4K.


## Known issues

1. If you're using a tiling window manager (e.g., Pop Shell) and have more than two windows open, the browser window with Stadia on, won't be maximized. I suggest that you bind a keyboard shortcut to the "Toggle fullscreen mode".

2. Sometimes, although `xrandr` reports your display resolution as 4K, the browser does not pick this up. In this case, simply run the `stadia on` and `stadia off` commands until the browser recognize it. Opening a new browser session may help too. 

You can check your browser resolution [here](https://duckduckgo.com/?q=display+resolution&ia=answer).
