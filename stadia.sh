#!/usr/bin/env bash

declare -r this="$0"
declare -l arg1="$1"
attempts="$2"

primary="$(xrandr -q | awk '/primary/ {print $1}')"
primary_resolution=$(xrandr -q | grep -i -m 1 'primary' -A 25 | grep -i -m 1 '*' | awk '{print $1}')
current_resolution=$(xrandr -q | grep -i 'primary' | grep -io '[0-9]\{1,4\}x[0-9]\{1,4\}' | head -1)
connected="$(xrandr -q | awk '/ connected/ {print $1}')"

if [ -n "$attempts" ] && [ "$attempts" -ge "5" ]; then
    echo -e '\e[1;31m Attempts exhausted: failed to set 4K resolution.\e[0m'
    exit 1
fi

stadia_tab() {
    window_name="$1"

    window_tab_id=$(xdotool search --onlyvisible --name "$window_name" | head -1)
    if [ -z "$window_tab_id" ]; then
        exit 1
    fi

    current=0
    browsers=('--class chrome' '--class brave' '--name chromium')
    browser=$(xdotool search --onlyvisible ${browsers[$current]})
    while :; do

        id=$(echo "$browser" | grep "$stadia_tab_id")
        if [ -n "$browser" ] && [ -n "$id" ]; then
            window_tab_id="$id"
            break
        fi

        current=$((current + 1))
        if [ "$current" -eq "${#browsers[@]}" ]; then
            echo -e '\e[1;31mCould not find Stadia instance.\e[0m'
            exit 1
        fi

        browser=$(xdotool search --onlyvisible ${browsers[$current]})

    done

    echo "$window_tab_id"
}

if [ "$arg1" = "on" ]; then

    if ! autorandr --detected | grep -iq 'stadia'; then
        autorandr --save stadia
    fi

    for screen in $connected; do
        if [ ! "$screen" = "$primary" ]; then
            xrandr --output "$screen" --off
        fi
    done

    xrandr --output "$primary" --mode "$primary_resolution" --refresh 240.00 --panning 3840x2160

    if [ ! "$(xrandr -q | awk '/primary/ {print $4}' | awk -F+ '{print $1}')" = "3840x2160" ]; then
        attempts=$([ -z "$attempts" ] && (echo 0) || (echo "$attempts"))
        if ! $this on $((attempts + 1)); then
            exit 1
        fi
    fi

    exit 0
elif [ "$arg1" = "off" ]; then

    autorandr --change stadia

    if ! xdotool search --onlyvisible --name "Play - Stadia" windowsize 100% 100%; then
        echo "Attempting to maximize window..."
	echo "Could not find Stadia instance. Is the browser window visible?"
    fi

elif [ "$arg1" = "vp9" ]; then
    
    echo -e '\e[1;33mAttempting to enforce VP9 encoding...\e[0m'

    stadia_tab_id=$(stadia_tab 'Play - Stadia')
    if [ -z "$stadia_tab_id" ]; then
        echo -e '\e[1;31mCould not find Stadia instance.\e[0m'
        exit 1
    fi

    printf "\e[1;33m%-40s %20s\e[0m\r" 'Latching onto browser window...' '[1/5]'
    xdotool windowfocus --sync "$stadia_tab_id"

    printf "\e[1;33m%-40s %20s\e[0m\r" 'Opening console...' '[2/5]'
    xdotool key Control_L+Shift+J

    sleep 1
    xdotool key Tab

    sleep 0.1
    printf "\e[1;33m%-40s %20s\e[0m\r" 'Typing command...' '[3/5]'
    xdotool type --delay 0 --clearmodifiers "localStorage.setItem(\"video_codec_implementation_by_codec_key\", '{\"vp9\":\"ExternalDecoder\"}');"

    printf "\e[1;33m%-40s %20s\e[0m\r" 'Submitting...' '[4/5]'
    xdotool key Return

    printf "\e[1;33m%-40s %20s\e[0m\n" 'Closing console...' '[5/5]'
    xdotool key Control_L+Shift+J

    echo -e '\e[1;32mDone.\e[0m'

elif [ "$arg1" = "performance" ]; then
    
    echo -e '\e[1;33mAttempting to enable 4K option...\e[0m'

    stadia_tab_id=$(stadia_tab 'Settings - Stadia')
    if [ -z "$stadia_tab_id" ]; then
        echo -e '\e[1;31mCould not find Stadia instance.\e[0m'
        exit 1
    fi

    printf "\e[1;33m%-40s %20s\e[0m\r" 'Latching onto browser window...' '[1/5]'
    xdotool windowfocus --sync "$stadia_tab_id"

    printf "\e[1;33m%-40s %20s\e[0m\r" 'Opening console...' '[2/5]'
    xdotool key Control_L+Shift+J

    sleep 1

    xdotool key Tab

    sleep 0.1
    printf "\e[1;33m%-40s %20s\e[0m\r" 'Typing command...' '[3/5]'
    xdotool type --delay 0 --clearmodifiers 'document.getElementsByClassName("sx2eZe QAAyWd aKIhz OWB6Me")[0].setAttribute("data-disabled", false);'

    printf "\e[1;33m%-40s %20s\e[0m\r" 'Submitting...' '[4/5]'
    xdotool key Return

    printf "\e[1;33m%-40s %20s\e[0m\n" 'Closing console...' '[5/5]'
    xdotool key Control_L+Shift+J

    echo -e '\e[1;32mDone.\e[0m'

elif [ "$arg1" = "status" ]; then

    if [ "$(xrandr -q | grep -c '*')" = "1" ] && [ "$current_resolution" = "3840x2160" ]; then
        echo "On"
    else
        echo "Off"
        exit 1
    fi
else
    echo "Expected 'on', 'off', 'vp9', 'performance' or 'status'. Found ($arg1)"
    exit 1
fi



